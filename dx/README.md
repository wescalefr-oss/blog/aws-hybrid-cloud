## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| dx\_allowed\_prefix\_association | direct connect gateway allowed prefix pour l'association | `list` | n/a | yes |
| dx\_amazon\_side\_asn | border gateway protocol (BGP) autonomous system number pour le dx | `string` | n/a | yes |
| dx\_bandwidth | direct connect bandwidth | `string` | n/a | yes |
| dx\_location | l'id de la localisation du direct connect donnée par le sous traitant de aws comme https://www.equinix.fr/ | `string` | n/a | yes |
| dx\_name | le nom du direct connect | `string` | n/a | yes |
| transit\_gateway\_id | l'id de la transit gateway | `string` | n/a | yes |
| vif\_address\_family | direct connect virtual interface address family ipv4 / ipv6 | `string` | n/a | yes |
| vif\_amazon\_side\_asn | border gateway protocol (BGP) autonomous system number pour la vif | `string` | n/a | yes |
| vif\_vlan\_tag | direct connect virtual interface vlan tag | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| attachment | n/a |
| transit\_gateway\_route\_table\_id | n/a |

