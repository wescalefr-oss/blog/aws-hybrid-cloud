resource "aws_dx_connection" "common" {
  name      = var.dx_name
  bandwidth = var.dx_bandwidth
  location  = var.dx_location
  tags      = local.tags
}

resource "aws_dx_gateway" "common" {
  name            = "first-dx-gateway"
  amazon_side_asn = var.dx_amazon_side_asn
}

resource "aws_dx_gateway_association" "common" {
  dx_gateway_id         = aws_dx_gateway.common.id
  associated_gateway_id = var.transit_gateway_id
  allowed_prefixes      = var.dx_allowed_prefix_association
}

resource "aws_dx_transit_virtual_interface" "common" {
  name           = "direct-connect-transit-virtual-interface"
  connection_id  = aws_dx_connection.common.id
  vlan           = var.vif_vlan_tag
  address_family = var.vif_address_family
  bgp_asn        = var.vif_amazon_side_asn
  dx_gateway_id  = aws_dx_gateway.common.id
}

data "aws_ec2_transit_gateway_dx_gateway_attachment" "dx" {
  transit_gateway_id = var.transit_gateway_id
  dx_gateway_id      = aws_dx_gateway.common.id
}


resource "aws_ec2_transit_gateway_route_table" "dx" {
  transit_gateway_id = var.transit_gateway_id
  tags = merge(
    map("Name", "direct-connect-tgw-rtbl"),
    local.tags
  )
}

resource "aws_ec2_transit_gateway_route_table_association" "dx" {
  transit_gateway_attachment_id  = var.transit_gateway_id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.dx.id
}


resource "aws_ec2_transit_gateway_route_table_propagation" "dx" {
  transit_gateway_attachment_id  = var.transit_gateway_id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.dx.id
}


