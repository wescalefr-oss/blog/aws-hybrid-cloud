variable "transit_gateway_id" {
  description = "l'id de la transit gateway"
  type        = string
}

variable "dx_name" {
  description = "le nom du direct connect"
  type        = string
}

variable "dx_amazon_side_asn" {
  description = "border gateway protocol (BGP) autonomous system number pour le dx"
  type        = string
}

variable "dx_bandwidth" {
  description = "direct connect bandwidth"
  type        = string
}

variable "dx_allowed_prefix_association" {
  description = "direct connect gateway allowed prefix pour l'association"
  type        = list
}

variable "dx_location" {
  description = "l'id de la localisation du direct connect donnée par le sous traitant de aws comme https://www.equinix.fr/"
  type        = string
}

variable "vif_vlan_tag" {
  description = "direct connect virtual interface vlan tag"
  type        = string
}

variable "vif_address_family" {
  description = "direct connect virtual interface address family ipv4 / ipv6"
  type        = string
}

variable "vif_amazon_side_asn" {
  description = "border gateway protocol (BGP) autonomous system number pour la vif"
  type        = string
}

locals {
  tags = {
    Environment = "infra"
    Project     = "aws"
    Owner       = "admin"
    Stack       = "dx"
    Cost        = "networking"
  }
}
