variable "transit_gateway_id" {
  description = "l'id de la transit gateway"
  type        = string
}

variable "customer_gateway_ip" {
  description = "l'adresse ip de l'interface externe de la gateway"
  type        = string
}

variable "customer_gateway_bgp_asn" {
  description = "border gateway protocol (BGP) autonomous system number pour le vpn"
  type        = number
}

variable "customer_gateway_type" {
  description = "le type de la customer gateway le seul type supporter par AWS jusqu'a maintenant est ipsec.1"
  type = string
}

locals {
  tags = {
    Environment = "infra"
    Project     = "aws"
    Owner       = "admin"
    Stack       = "vpn"
    Cost        = "networking"
  }
}
