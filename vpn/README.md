## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| customer\_gateway\_bgp\_asn | border gateway protocol (BGP) autonomous system number pour le vpn | `number` | n/a | yes |
| customer\_gateway\_ip | l'adresse ip de l'interface externe de la gateway | `string` | n/a | yes |
| customer\_gateway\_type | le type de la customer gateway le seul type supporter par AWS jusqu'a maintenant est ipsec.1 | `string` | n/a | yes |
| transit\_gateway\_id | l'id de la transit gateway | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| attachment | n/a |
| transit\_gateway\_route\_table\_id | n/a |

