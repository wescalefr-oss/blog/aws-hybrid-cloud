output "attachment" { value = data.aws_ec2_transit_gateway_vpn_attachment.vpn }
output "transit_gateway_route_table_id" { value = aws_ec2_transit_gateway_route_table.vpn }
