resource "aws_customer_gateway" "vpn" {
  bgp_asn    = var.customer_gateway_bgp_asn
  ip_address = var.customer_gateway_ip
  type       = var.customer_gateway_type
  tags = merge(
    map("Name", "vpn-custom-gateway"),
    local.tags
  )
}

resource "aws_vpn_connection" "vpn" {
  customer_gateway_id = aws_customer_gateway.vpn.id
  transit_gateway_id  = var.transit_gateway_id
  type                = aws_customer_gateway.vpn.type
  tunnel1_preshared_key = "jD48w3rfbTWCJutfltAb"
  tunnel2_preshared_key = "qrQmBwpyGpZlWeWRuH6T"
  static_routes_only  = false
  tags = merge(
    map("Name", "vpn-site-to-site-connection"),
    local.tags
  )
}

resource "aws_ec2_transit_gateway_route_table" "vpn" {
  transit_gateway_id = var.transit_gateway_id
  tags = merge(
    map("Name", "vpn-tgw-rtbl"),
    local.tags
  )
}

data "aws_ec2_transit_gateway_vpn_attachment" "vpn" {
  transit_gateway_id = var.transit_gateway_id
  vpn_connection_id  = aws_vpn_connection.vpn.id
}

resource "aws_ec2_transit_gateway_route_table_association" "vpn" {
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpn_attachment.vpn.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.vpn.id
}

resource "aws_ec2_transit_gateway_route_table_propagation" "vpn" {
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpn_attachment.vpn.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.vpn.id
}



