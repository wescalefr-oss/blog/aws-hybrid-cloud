variable "transit_gateway_id" {
  description = "l'id de la transit gateway"
  type        = string
}

variable "vpc_cidr_block" {
  description = "le cidr block utilisé pour le vpc"
  type        = string
}

variable "on_premise_cidr_block" {
  description = "le cidr block utilisé pour le on premise réseau"
  type        = string
}

variable "vpc_name" {
  description = "le nom utiliser pour identifier chaque vpc"
}
variable "availability_zone" {
  description = "la zone de disponibilité pour chaque sous réseau"
  type        = string
  default     = "eu-west-3a"
}

locals {
  tags = {
    Environment = "infra"
    Project     = "aws"
    Owner       = "admin"
    Stack       = "vpc"
    Cost        = "networking"
  }
}
