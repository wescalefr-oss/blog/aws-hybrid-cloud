## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| availability\_zone | la zone de disponibilité pour chaque sous réseau | `string` | `"eu-west-3a"` | no |
| on\_premise\_cidr\_block | le cidr block utilisé pour le on premise réseau | `string` | n/a | yes |
| transit\_gateway\_id | l'id de la transit gateway | `string` | n/a | yes |
| vpc\_cidr\_block | le cidr block utilisé pour le vpc | `string` | n/a | yes |
| vpc\_name | le nom utiliser pour identifier chaque vpc | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| attachment | n/a |
| subnet | n/a |
| transit\_gateway\_route\_table\_id | n/a |
| vpc | n/a |

