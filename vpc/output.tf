output "vpc" { value = aws_vpc.main }
output "attachment" { value = aws_ec2_transit_gateway_vpc_attachment.vpc }
output "subnet" { value = aws_subnet.public }
output "transit_gateway_route_table_id" { value = aws_ec2_transit_gateway_route_table.vpc }
