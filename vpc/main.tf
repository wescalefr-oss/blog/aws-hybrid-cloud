resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = merge(
    {
      "Name" = "${var.vpc_name}"
    },
    local.tags,
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    {
      "Name" = "common-itgw"
    },
    local.tags,
  )
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    {
      "Name" = "common-route-table-${var.vpc_name}"
    },
    local.tags,
  )
}

resource "aws_route" "main" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_route" "onpremise" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = var.on_premise_cidr_block
  transit_gateway_id     = var.transit_gateway_id
}


resource "aws_main_route_table_association" "main" {
  route_table_id = aws_route_table.main.id
  vpc_id         = aws_vpc.main.id
}

resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(var.vpc_cidr_block, 8, 1)
  availability_zone = var.availability_zone
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.main.id
}

resource "aws_network_acl" "main" {
 vpc_id = aws_vpc.main.id
 subnet_ids = [aws_subnet.public.id]

 egress {
   protocol   = "tcp"
   rule_no    = 100
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 0
   to_port    = 65535
 }

egress {
   protocol   = "udp"
   rule_no    = 100
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 0
   to_port    = 65535
 }

 ingress {
   protocol   = "tcp"
   rule_no    = 100
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 443
   to_port    = 443
 }

ingress {
   protocol   = "tcp"
   rule_no    = 100
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 53
   to_port    =53
 }



ingress {
   protocol   = "udp"
   rule_no    = 100
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 53
   to_port    = 53
 }



 ingress {
   protocol   = "tcp"
   rule_no    = 200
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 80
   to_port    = 80
 }

 ingress {
   protocol   = "tcp"
   rule_no    = 300
   action     = "allow"
   cidr_block = "0.0.0.0"
   from_port  = 22
   to_port    = 22
 }

 ingress {
   protocol   = "tcp"
   rule_no    = 9000
   action     = "deny"
   cidr_block = "0.0.0.0"
   from_port  = 0
   to_port    = 65535
 }

 ingress {
   protocol = "icmp"
   rule_no = 100
   action = "allow"
   cidr_block = "0.0.0.0/0"
   from_port = -1
   to_port = -1
   icmp_type = -1
   icmp_code = -1
 }

 tags = {
   Name = "main"
 }
}



resource "aws_ec2_transit_gateway_vpc_attachment" "vpc" {
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = aws_vpc.main.id
  subnet_ids         = [aws_subnet.public.id]
  tags = merge(
    {
      "Name" = "transit-gateway-attachement-${var.vpc_name}"
    },
    local.tags,
  )

  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
}

resource "aws_ec2_transit_gateway_route_table" "vpc" {
  transit_gateway_id = var.transit_gateway_id
  tags = merge(
    map("Name", "tgw-rt-${var.vpc_name}"),
    local.tags
  )
}

resource "aws_ec2_transit_gateway_route_table_association" "vpc" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.vpc.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.vpc.id
}

resource "aws_ec2_transit_gateway_route_table_propagation" "vpc" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.vpc.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.vpc.id
}

resource "aws_cloudwatch_log_group" "flowlogs" {
  name = "flowlog"
}

resource "aws_iam_role" "flowlogs" {
  name = "flow-log-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "flowlogs" {
  name = "flow-log-policy"
  role = aws_iam_role.flowlogs.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_flow_log" "vpc_flowlogs" {
  iam_role_arn    = aws_iam_role.flowlogs.arn
  log_destination = aws_cloudwatch_log_group.flowlogs.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.main.id
}

resource "aws_security_group" "default" {
  name   = "dns-resolver-endpoint-sg"
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      "Name" = "dns-resolver-endpoint-sg"
    },
    local.tags,
  )

  ingress {
    protocol    = "udp"
    from_port   = 53
    to_port     = 53
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 53
    to_port     = 53
    cidr_blocks = ["10.0.0.0/8"]
  }
  
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["10.0.0.0/8"]
  }
}

resource "aws_route53_resolver_endpoint" "outbound" {
  name      = "outbound-endpoint"
  direction = "OUTBOUND"

  security_group_ids = [
    aws_security_group.default.id,
  ]

  ip_address {
    subnet_id = aws_subnet.public.id
  }

  tags = merge(
    map("Name", "outbound-endpoint"),
    local.tags
  )
}

resource "aws_route53_resolver_rule" "example" {
  domain_name          = "example.com"
  name                 = "example"
  rule_type            = "FORWARD"
  resolver_endpoint_id = aws_route53_resolver_endpoint.outbound.id

  target_ip {
    ip = "1.2.3.4"
  }

  tags = merge(
    map("Name", "outbound-rule"),
    local.tags
  )
}

resource "aws_route53_resolver_rule_association" "example" {
  resolver_rule_id = aws_route53_resolver_rule.example.id
  vpc_id           = aws_vpc.main.id
}
