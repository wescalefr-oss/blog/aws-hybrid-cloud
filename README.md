## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| amazon\_side\_asn | The Autonomous System Number (ASN) for the Amazon side of the gateway. By default the TGW is created with the current default Amazon ASN. | `string` | `"64510"` | no |
| description | Description of the EC2 Transit Gateway | `string` | n/a | yes |
| enable\_auto\_accept\_shared\_attachments | Whether resource attachment requests are automatically accepted | `bool` | `true` | no |
| enable\_default\_route\_table\_association | Whether resource attachments are automatically associated with the default association route table | `bool` | `false` | no |
| enable\_default\_route\_table\_propagation | Whether resource attachments automatically propagate routes to the default propagation route table | `bool` | `false` | no |
| enable\_dns\_support | Should be true to enable DNS support in the TGW | `bool` | `true` | no |
| enable\_vpn\_ecmp\_support | Whether VPN Equal Cost Multipath Protocol support is enabled | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| transit\_gateway\_id | n/a |

