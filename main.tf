resource "aws_ec2_transit_gateway" "common" {
  description                     = var.description
  amazon_side_asn                 = var.amazon_side_asn
  default_route_table_association = var.enable_default_route_table_association ? "enable" : "disable"
  default_route_table_propagation = var.enable_default_route_table_propagation ? "enable" : "disable"
  auto_accept_shared_attachments  = var.enable_auto_accept_shared_attachments ? "enable" : "disable"
  vpn_ecmp_support                = var.enable_vpn_ecmp_support ? "enable" : "disable"
  dns_support                     = var.enable_dns_support ? "enable" : "disable"

  tags = merge(
    map("Name", "common-transit-gateway"),
    local.tags
  )
}

module "tvpc" {
  source                = "./vpc"
  vpc_name              = "transit-vpc"
  vpc_cidr_block        = "10.10.0.0/16"
  on_premise_cidr_block = "172.16.0.0/16"
  transit_gateway_id    = aws_ec2_transit_gateway.common.id
}

module "tvpn" {
  source                   = "./vpn"
  transit_gateway_id       = aws_ec2_transit_gateway.common.id
  customer_gateway_ip      = "172.83.124.10"
  customer_gateway_type    = "ipsec.1"
  customer_gateway_bgp_asn = 64512
}

module "tdx" {
  source                        = "./dx"
  transit_gateway_id            = aws_ec2_transit_gateway.common.id
  dx_name                       = "common-dx"
  dx_amazon_side_asn            = "64514"
  dx_bandwidth                  = "1Gbps"
  dx_location                   = "ZRF5B"
  dx_allowed_prefix_association = ["172.24.0.0/20", "172.25.0.0/20", ]
  vif_vlan_tag                  = "4094"
  vif_address_family            = "ipv4"
  vif_amazon_side_asn           = "64516"
}

resource "aws_ec2_transit_gateway_route_table_propagation" "vpc_vpn" {
  transit_gateway_attachment_id  = module.tvpn.attachment.id
  transit_gateway_route_table_id = module.tvpc.transit_gateway_route_table_id.id
}

resource "aws_ec2_transit_gateway_route_table_propagation" "vpc_dx" {
  transit_gateway_attachment_id  = module.tdx.attachment.id
  transit_gateway_route_table_id = module.tvpc.transit_gateway_route_table_id.id
}

resource "aws_ec2_transit_gateway_route_table_propagation" "dx_vpc" {
  transit_gateway_attachment_id  = module.tvpc.attachment.id
  transit_gateway_route_table_id = module.tdx.transit_gateway_route_table_id.id
}

resource "aws_ec2_transit_gateway_route_table_propagation" "vpn_vpc" {
  transit_gateway_attachment_id  = module.tvpc.attachment.id
  transit_gateway_route_table_id = module.tvpn.transit_gateway_route_table_id.id
}